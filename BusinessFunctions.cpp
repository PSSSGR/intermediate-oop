/*
 * Filename:	BusinessFunctions.cpp 
 * Name:		Sri Padala
 * Description:	This program takes input from user and builds up the statistics of their hotdog stands. 
 */

#include<iostream>
#include<random>
#include<vector>
#include<iomanip>
#include"hotdogstand.hpp"
#include"money.hpp"
#include"BusinessFunctions.hpp"
namespace  MyAwesomeBusiness{
	int numdays=0;
    void runSimulation(std::vector<HotdogStand> &franchises, int days){
		numdays=days;
        for(int i=0;i<days;i++){
            for(unsigned int z =0;z<franchises.size();z++){
				franchises[z].dailyReset();
				if((franchises[z].getPrice().getPennies())<=350){
			    	std::random_device rd;
			    	std::mt19937 generator(rd());
			    	std::uniform_int_distribution<int> uniDist(20, 60);//Gives random number in the given range.
			    	int sales = uniDist(generator);
			    	for (auto s = 1; s <= sales; ++s){
                     	franchises[z].sellHotdog(); //Increments all appropriate data members accordingly 
                 	}
			    }else{
			    	std::random_device rd;
			    	std::mt19937 generator(rd());
			    	std::uniform_int_distribution<int> uniDist(1, 30);//Gives random number in the given range.
			    	int sales = uniDist(generator);
			    	for (auto j = 1; j <= sales; ++j){
			    	    franchises[z].sellHotdog();// Increments all appropriate data members accordingly 
			    	}
			    }
				franchises[z].replenishSupplies(); 
				franchises[z].payWorker();	
	    	} 
			printRundown(franchises);	

        }
    }
    void printRundown(const std::vector<HotdogStand> &franchises){
		static int counter=0;
		counter++;
        std::cout<<"\nStand	Sales	Price	Revenue Remaining Cash\n";
        std::cout<<"=====	=====	=====	======= ==============\n";
        for(unsigned int i =0; i<franchises.size(); i++){
		    Money temp=franchises[i].MyAwesomeBusiness::HotdogStand::getPrice();
          if(temp.getPennies()==350){//Checks for the general price.
		    	int sales=franchises[i].getDailyDogsSold();
		    	std::cout << std::setw(5) << std::right << std::setfill(' ') <<i+1<< std::setw(8) << std::right<< std::setfill(' ')<<sales;
		    	std::cout << std::setw(8) << std::right << std::setfill(' ');
		    	std::cout<<temp;//Prints the price of the hotdog.
                std::cout<<std::setw(10) << std::right << std::setfill(' ');
		    	Money temp3=franchises[i].getCash();
                std::cout<<temp3;//Prints the stand revenue.
				std::cout<<std::setw(15) << std::right << std::setfill(' ');
		    	Money tempa=franchises[i].getStandRevenue();
                std::cout<<tempa;//Prints the stand revenue.
                std::cout<<"\n";
            }else{
                int sales=franchises[i].MyAwesomeBusiness::HotdogStand::getDailyDogsSold();
                std::cout<< std::setw(5) << std::right << std::setfill(' ') <<i+1<< std::setw(8) << std::right<<std::setfill(' ') <<sales;
			    Money temp4=franchises[i].getPrice();
			    std::cout << std::setw(8) << std::right << std::setfill(' ');
                std::cout<<temp4;//Prints the price of the hotdog.
			    std::cout<<std::setw(10) << std::right << std::setfill(' ');
			    Money temp5=franchises[i].getCash();
                std::cout<<temp5;//Prints the stand revenue.
				std::cout<<std::setw(15) << std::right << std::setfill(' ');
		    	Money tempb=franchises[i].getStandRevenue();
                std::cout<<tempb;//Prints the stand revenue.
                std::cout<<"\n";
            }
			
        }
		if(counter==numdays){
        		Money temp6;
        		temp6=MyAwesomeBusiness::HotdogStand::getTotalRevenue();
        		std::cout<<"\nTotals    ";
        		std::cout<<std::setw(3) << std::right << std::setfill(' ')<<MyAwesomeBusiness::HotdogStand::getTotalHotdogsSold();
    			std::cout << std::setw(18) << std::right << std::setfill(' ');
      		  	std::cout<<temp6;//Prints the total revenue of the hotdog business.
				std::cout << std::setw(15) << std::right << std::setfill(' ');
      		  	std::cout<<MyAwesomeBusiness::HotdogStand::getTotalmoney();	
    			std::cout<<"\n";
     			std::cout<<"# of Stands: ";
     			std::cout<<MyAwesomeBusiness::HotdogStand::getNumStands()<<std::endl;
		}

    }
}