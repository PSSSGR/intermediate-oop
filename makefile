
 # Filename:	makefile 
 # Name:		Sri Padala
 # Description:	This program takes input from user and builds up the statistics of their hotdog stands. 
GXX=g++
GFLAGS=-Wall -std=c++11 -o
CFLAGS=-Wall -std=c++11 -c
PROG=hw04

PROG:main.cpp money.o hotdogstand.o BusinessFunctions.o#compiles the main.cpp and links all object files.
	$(GXX) $(GFLAGS) $(PROG) main.cpp money.o hotdogstand.o BusinessFunctions.o 
	
money.o:money.cpp money.hpp#creates the object file for the money file and compiles it but does not link it.
	$(GXX) $(CFLAGS) money.cpp
	
hotdogstand.o:hotdogstand.cpp hotdogstand.hpp#creates the object file for the hotdog file and compiles it but does not link it.
	$(GXX) $(CFLAGS) hotdogstand.cpp
	
BusinessFunctions.o:BusinessFunctions.cpp BusinessFunctions.hpp#creates the object file for the nonmember file and compiles it but does not link it.
	$(GXX) $(CFLAGS) BusinessFunctions.cpp
	

.PHONY:clean

clean:#removes the object and executable file.
	rm -f *.o core $(PROG)
